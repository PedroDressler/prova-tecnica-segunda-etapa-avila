//
//  CelulaMusica.swift
//  prova-tecnica-segunda-etapa-avila
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

class CelulaMusica: UITableViewCell {

    
    @IBOutlet var lblAlbum: UILabel!
    @IBOutlet var lblCantor: UILabel!
    @IBOutlet var lblMusica: UILabel!
    @IBOutlet var imgCapa: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
