//
//  ViewController.swift
//  prova-tecnica-segunda-etapa-avila
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Musica {
    let nomeMusica: String
    let nomeAlbum: String
    let nomeCantor: String
    let imgPequena: String
    let imgGrande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusicas:[Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusicas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! CelulaMusica
        let musica = self.listaDeMusicas[indexPath.row]
        cell.lblMusica.text = musica.nomeMusica
        cell.lblAlbum.text = musica.nomeAlbum
        cell.lblCantor.text = musica.nomeCantor
        cell.imgCapa.image = UIImage(named: musica.imgPequena)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusicas[indice]
        
        detalhesViewController.nomeImagem = musica.imgGrande
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.nomeCantor
    }

    @IBOutlet var tabela: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabela.dataSource = self
        self.tabela.delegate = self
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Pontos Cardeais", nomeAlbum: "Álbum Vivo", nomeCantor: "Alceu Valença", imgPequena: "capa_alceu_pequeno", imgGrande: "capa_alceu_grande"))
        self.listaDeMusicas.append(Musica(nomeMusica: "Menor Abandonado", nomeAlbum: "Álbum Patota de Cosme", nomeCantor: "Zeca Pagodinho", imgPequena: "capa_zeca_pequeno", imgGrande: "capa_zeca_grande"))
        self.listaDeMusicas.append(Musica(nomeMusica: "Tiro ao Álvaro", nomeAlbum: "Álbum Adoniran Barbosa e Convidados", nomeCantor: "Adoniran Barbosa", imgPequena: "capa_adoniran_pequeno", imgGrande: "capa_adoniran_grande"))
        // Do any additional setup after loading the view.
    }


}

